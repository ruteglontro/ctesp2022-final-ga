﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Code.Models
{
    public class Cliente
    {
        [Key]
        public int ClienteId { get; set; }
        [ForeignKey("ClienteId")]
        public ICollection<ContaBancaria> ContasBancarias { get; set; }
     

        public string NomeCliente { get; set; }
        public string Morada { get; set; }
        public string Contrato { get; set; }
        public Cliente() { }
        public Cliente (int clientId, string nomeCliente, string morada, string contrato)
        { 
            this.ClienteId = clientId;
            this.NomeCliente = nomeCliente;
            this.Morada = morada;
            this.Contrato = contrato;
        }
        public Cliente(string nomeCliente, string morada, string contrato)
        {
            this.NomeCliente = nomeCliente;
            this.Morada = morada;
            this.Contrato = contrato;
        }
    }
}
