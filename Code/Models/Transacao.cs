﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Code.Models
{
    public class Transacao
    {
        [Key]
        public int TransacaoId { get; set; }
        //Foreign key for ContaId
        public int ContaId { get; set; }
        public ContaBancaria ContaBancaria { get; set; }
        //Foreign key for TipoTransacao
        public int TipoTransacaoId { get; set; }
        public TipoTransacao TipoTransacao { get; set; }
        //
        public DateTime Dia { get; set; }
        public double Valor { get; set; }
 
        public Transacao() { }

        public Transacao(int contaId, int tipoTransacaoId, DateTime dia, double valor)
        {

            this.ContaId = contaId;
            this.TipoTransacaoId = tipoTransacaoId;
            this.Dia = dia;
            this.Valor = valor;
        }
        public Transacao(int transacaoId, int contaId, int tipoTransacaoId, DateTime dia, double valor)
        {
            this.TransacaoId = transacaoId;
            this.ContaId = contaId;
            this.TipoTransacaoId = tipoTransacaoId;
            this.Dia = dia;
            this.Valor = valor;
        }

        
        /**
         * Apenas para efeitos de testes
         */
        public Transacao(bool isDebt, int transacaoId, int contaId, int tipoTransacaoId, DateTime dia, double valor)
        {
            this.TransacaoId = transacaoId;
            this.ContaId = contaId;
            this.TipoTransacaoId = tipoTransacaoId;
            this.Dia = dia;
            this.Valor = valor;


            if (isDebt)
            {
                this.TipoTransacao = new TipoTransacao(1, "Débito");
            }
            else
            {
                this.TipoTransacao = new TipoTransacao(2, "Crédito");
            }
        }
    }
}
