﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Code.Models
{
    public class TipoTransacao
    {
        [Key]
        public int TipoId { get; set; }
        public string Descricao { get; set; }

        [ForeignKey("TipoTransacaoId")]
        public ICollection<Transacao> Transacoes { get; set; }


        public TipoTransacao() { }

        public TipoTransacao(int tipoId, string descricao)
        {
            this.TipoId = tipoId;
            this.Descricao = descricao;
        }
        public TipoTransacao(string descricao)
        {
            this.Descricao = descricao;
        }
    }
}
