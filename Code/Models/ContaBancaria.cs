﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Code.Models
{
    public class ContaBancaria
    {
        [Key]
        public int ContaId { get; set; }
        [ForeignKey("ContaId")]
        public ICollection<Transacao> Transacoes  { get; set; }

        //Foreign key for Client
        public int ClienteId { get; set; }        
        public Cliente Cliente { get; set; }
        //
        public int NumeroConta { get; set; }
        public string Iban{ get; set; }
        public double SaldoCorrente { get; set; }
     
        public ContaBancaria() { }

        public ContaBancaria(int contaId, int clientId, int numeroConta, string iban, double saldoCorrente )
        {
            this.ContaId= contaId;
            this.ClienteId = clientId;
            this.NumeroConta = numeroConta;
            this.Iban = iban;
            this.SaldoCorrente = saldoCorrente;
        }

        public ContaBancaria(int clientId, int numeroConta, string iban, double saldoCorrente)
        {
            this.ClienteId = clientId;
            this.NumeroConta = numeroConta;
            this.Iban = iban;
            this.SaldoCorrente = saldoCorrente;
        }
    }
}
