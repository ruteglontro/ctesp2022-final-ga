﻿using Code.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Code.Migrations
{
    public class Seed
    {
        Intro.BackEndContext db = new Intro.BackEndContext();

        List<TipoTransacao> tiposTransacao;
        List<Cliente> clientes;
        List<ContaBancaria> contasBancarias;
        List<Transacao> transacoes;


        public void populate()
        {

            populateTipoTransacao();
            populateCliente();
            populateContaBancaria();
            populateTransacao();

        }


        private void populateTipoTransacao()
        {
            // Tipo Transacao
            // Validar se existem registos
            tiposTransacao = db.TipoTransacao.ToList();

            if (tiposTransacao.Count == 0)
            {
                // Se não existirem, inserir
                tiposTransacao.Add(new TipoTransacao("Débito"));
                tiposTransacao.Add(new TipoTransacao("Crédito"));

                foreach (TipoTransacao tipoT in tiposTransacao)
                {
                    db.TipoTransacao.Add(tipoT);
                }
                db.SaveChanges();
            }

        }

        private void populateCliente()
        {
            // Cliente
            // Validar se existem registos
            clientes = db.Cliente.ToList();


            if (clientes.Count == 0)
            {
                // Se não existirem, inserir
                clientes.Add(new Cliente("António Santos", "Rua 31 de Janeiro, Porto", "Conta ordenado"));
                clientes.Add(new Cliente("Maria Silva", "Rua 1º de Maio, Santa Maria da Feira", "Conta ordenado"));
                clientes.Add(new Cliente("Sofia Andrade", "Avenida da Beira-Mar, Vila Nova de Gaia", "Conta poupança"));

                foreach (Cliente client in clientes)
                {
                    db.Cliente.Add(client);
                }
                db.SaveChanges();
            }
              

        }



        private void populateContaBancaria()
        {
            // Conta Bancária
            // Validar se existem registos
            contasBancarias = db.ContaBancaria.ToList();

            if (contasBancarias.Count == 0)
            {
                // int clientId, int numeroConta, string iban, double saldoCorrente

                // Se não existirem, inserir
                contasBancarias.Add(new ContaBancaria(clientes.ElementAt(0).ClienteId, 0009871031, "PT50000201231234567890154", 0));
                contasBancarias.Add(new ContaBancaria(clientes.ElementAt(1).ClienteId, 0002002385, "PT50000123456789101112131", 0));
                contasBancarias.Add(new ContaBancaria(clientes.ElementAt(2).ClienteId, 0004592904, "PT50000987654321998765432", 0));


                foreach (ContaBancaria conta in contasBancarias)
                {
                    db.ContaBancaria.Add(conta);
                }
 
                db.SaveChanges();
            }
        }

        private void populateTransacao()
        {
            // Transações
            // Validar se existem registos
            transacoes = db.Transacao.ToList();

            if (transacoes.Count == 0)
            {
                // Cliente 1
                transacoes.Add(new Transacao(clientes.ElementAt(0).ClienteId, tiposTransacao.ElementAt(0).TipoId, new DateTime(2022, 02, 03, 10, 10, 00), 50.70));
                transacoes.Add(new Transacao(clientes.ElementAt(0).ClienteId, tiposTransacao.ElementAt(1).TipoId, new DateTime(2022, 02, 06, 15, 35, 00), 400.00));
                transacoes.Add(new Transacao(clientes.ElementAt(0).ClienteId, tiposTransacao.ElementAt(0).TipoId, new DateTime(2022, 02, 12, 20, 50, 00), 30.50));
                // Cliente 2
                transacoes.Add(new Transacao(clientes.ElementAt(1).ClienteId, tiposTransacao.ElementAt(1).TipoId, new DateTime(2022, 02, 02, 11, 45, 00), 500.00));
                transacoes.Add(new Transacao(clientes.ElementAt(1).ClienteId, tiposTransacao.ElementAt(0).TipoId, new DateTime(2022, 02, 07, 17, 20, 00), 23.10));
                transacoes.Add(new Transacao(clientes.ElementAt(1).ClienteId, tiposTransacao.ElementAt(1).TipoId, new DateTime(2022, 02, 09, 19, 05, 00), 220.00));
                transacoes.Add(new Transacao(clientes.ElementAt(1).ClienteId, tiposTransacao.ElementAt(0).TipoId, new DateTime(2022, 02, 11, 21, 30, 00), 82.65));
                // Cliente 3 - conta poupança
                transacoes.Add(new Transacao(clientes.ElementAt(2).ClienteId, tiposTransacao.ElementAt(1).TipoId, new DateTime(2021, 11, 25, 17, 00, 00), 100.00));
                transacoes.Add(new Transacao(clientes.ElementAt(2).ClienteId, tiposTransacao.ElementAt(1).TipoId, new DateTime(2021, 12, 25, 18, 00, 00), 200.00));
                transacoes.Add(new Transacao(clientes.ElementAt(2).ClienteId, tiposTransacao.ElementAt(1).TipoId, new DateTime(2022, 01, 25, 16, 00, 00), 50.00));
                transacoes.Add(new Transacao(clientes.ElementAt(2).ClienteId, tiposTransacao.ElementAt(1).TipoId, new DateTime(2022, 02, 25, 19, 00, 00), 350.00));


                foreach (Transacao trans in transacoes)
                {
                    db.Transacao.Add(trans);
                }

                updateSaldoCorrente(); // A SUBSTITUIR PELO MÉTODO DO BUSINESS
                db.SaveChanges();
            }
        }

        private void updateSaldoCorrente()
        {

            if (transacoes.Count > 0  && contasBancarias.Count > 0)
            {
                foreach (ContaBancaria conta in contasBancarias)
                {
                    double saldo = 0.0;

                    foreach (Transacao trans in transacoes.FindAll(t => t.ContaId.Equals(conta.ContaId)))
                    {
                        if (trans.TipoTransacaoId == 2)
                        {
                            saldo += trans.Valor;
                        }
                        else
                        { 
                            saldo -= trans.Valor;
                        }

                    }

                    conta.SaldoCorrente = saldo;
                }
            }


        }

    }
}
