﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Code.Models;
using Code.Intro;

namespace Code.Bus
{
    public class BusCliente
    {
        private BackEndContext db;
        private List<Cliente> clienteList = new List<Cliente>();

        public BusCliente()
        {
            db = new BackEndContext();
            clienteList = db.Cliente.ToList();

            /*
            clienteList = new List<Cliente>();

            clienteList.Add(new Cliente(-1, "António Santos", "Rua 31 de Janeiro, Porto", "Conta ordenado"));
            clienteList.Add(new Cliente(-2, "Maria Silva", "Rua 1º de Maio, Santa Maria da Feira", "Conta ordenado"));
            */

        }

        public List<Cliente> Get(){
            return clienteList;
        }

        public Cliente GetId(int clientid)
        {
            IActionResult result;

            return clienteList.Where(x => x.ClienteId == clientid)
                              .FirstOrDefault();
        }

        public bool Create(Cliente cliente)
        {

            bool result;

            Cliente match = clienteList.Find((obj) => obj.ClienteId == cliente.ClienteId);

            if (match == null)
            {
                clienteList.Add(cliente);
                db.Cliente.Add(cliente);
                db.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool Update(int clientid, Cliente cliente)
        {
            if (clientid != cliente.ClienteId) return false;

            bool result = false;
            Cliente match = GetId(clientid);

            if (match != null)
            {
                clienteList[clienteList.IndexOf(match)] = cliente;
                db.Cliente.Update(cliente);
                db.SaveChanges();
                result = true;
            }
            
            return result;
        }

        public bool Delete(int clientid)
        {
            bool result = false;

            Cliente match = GetId(clientid);

            if (match != null)
            {
                clienteList.RemoveAt(clienteList.IndexOf(match));
                db.Cliente.Remove(match);
                db.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }
    }
}
