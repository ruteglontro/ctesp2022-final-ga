﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Code.Models;
using Code.Bus;
using Code.Intro;

namespace Code.Bus
{
    public class BusContaBancaria
    {
        private List<ContaBancaria> contaBancariaList;
        private BackEndContext db;

        public BusContaBancaria()
        {
            db = new BackEndContext();
            contaBancariaList = db.ContaBancaria.ToList();

            /*
            contaBancariaList = new List<ContaBancaria>();
            contaBancariaList.Add(new ContaBancaria(-1, -1, 0009871031, "PT50000201231234567890154", 0));
            contaBancariaList.Add(new ContaBancaria(-2, -2, 0002002385, "PT50000123456789101112131", 0));
            contaBancariaList.Add(new ContaBancaria(-3, -3, 0004592904, "PT50000987654321998765432", 0));
            */
        }

        public List<ContaBancaria> Get()
        {

            return contaBancariaList.ToList();
        }


        public ContaBancaria GetId(int contaid)
        {
           return contaBancariaList.Where(x => x.ContaId == contaid).FirstOrDefault();
        }

        [HttpPost]
        public bool Create(ContaBancaria ContaBancaria)
        {

            bool result = false;

            ContaBancaria match = GetId(ContaBancaria.ContaId);

            if (match == null)
            {
                contaBancariaList.Add(ContaBancaria);
                db.ContaBancaria.Add(ContaBancaria);
                db.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }
       
        public bool Update(int contaid, ContaBancaria ContaBancaria)
        {
            bool result;

            if (contaid != ContaBancaria.ContaId) return false;

            ContaBancaria match = GetId(contaid);
            
            if (match != null)
            {
                contaBancariaList[contaBancariaList.IndexOf(match)] = ContaBancaria;
                db.ContaBancaria.Update(ContaBancaria);
                db.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool Delete(int contaid)
        {
            bool result;

            ContaBancaria match = GetId(contaid);

            if (match != null)
            {
                contaBancariaList.RemoveAt(contaBancariaList.IndexOf(match));
                db.ContaBancaria.Remove(match);
                db.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }

    }
}
