﻿using Code.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Code.Intro;

namespace Code.Bus
{
    public class BusTipoTransacao
    {

        private BackEndContext db;
        private List<TipoTransacao> listTipoTransacoes = new List<TipoTransacao>();

        public BusTipoTransacao()
        {
            BackEndContext db = new BackEndContext();
            listTipoTransacoes = db.TipoTransacao.ToList();
            
            /*
            listTipoTransacoes = new List<TipoTransacao>();
            listTipoTransacoes.Add(new TipoTransacao(-1, "Débito"));
            listTipoTransacoes.Add(new TipoTransacao(-2, "Crédito"));
            */
        }

        public List<TipoTransacao> Get()
        {
            return listTipoTransacoes;
        }

        public TipoTransacao GetId(int tipoId)
        {
            return listTipoTransacoes.Where(x => x.TipoId == tipoId)
                                     .FirstOrDefault();
        }

        public bool Create(TipoTransacao tipoTransacao)
        {

            TipoTransacao match = GetId(tipoTransacao.TipoId);

            if (match == null)
            {
                listTipoTransacoes.Add(tipoTransacao);
                db.TipoTransacao.Add(tipoTransacao);
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Update(int tipoId, TipoTransacao tipoTransacao)
        {
            if (tipoId != tipoTransacao.TipoId)  return false;
            

            TipoTransacao match = GetId(tipoId);

            if (match != null)
            {
                listTipoTransacoes[listTipoTransacoes.IndexOf(match)] = tipoTransacao;
                db.TipoTransacao.Update(tipoTransacao);
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(int tipoId)
        {

            TipoTransacao match = GetId(tipoId);

            if (match != null)
            {
                listTipoTransacoes.RemoveAt(listTipoTransacoes.IndexOf(match));
                db.TipoTransacao.Remove(match);
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
