﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Code.Models;
using Code.Intro;

namespace Code.Bus
{
    public class BusTransacao
    {
        private BackEndContext dbContext;
       
        private List<Transacao> transacaoList;

        public BusTransacao()
        {
            dbContext = new BackEndContext();
            /*
            transacaoList = new List<Transacao>();
            transacaoList.Add(new Transacao(true, -1, -1, 0, DateTime.Now, 10f));
            transacaoList.Add(new Transacao(true, -1, -1, 0, DateTime.Now, 10f));
            transacaoList.Add(new Transacao(false, -2, -1, 0, DateTime.Now, 40f));
            transacaoList.Add(new Transacao(false, -3, -1, 0, DateTime.Now, 40f));
            */
        }

        public List<Transacao> GetAll()
        {
            //return transacaoList;
            return dbContext.Transacao.ToList();
        }

        public Transacao GetTransacao(int transactionId)
        {
           return dbContext.Transacao.Where(transaction => transaction.TransacaoId == transactionId)
                                         .FirstOrDefault();
        }

        public double GetTotalCredito(int contaId)
        {
             
            //List<Transacao> transacoesCredito = transacaoList.Where(transaction => transaction.ContaId == contaId && transaction.TipoTransacao.Descricao == "Crédito")
            //                                                 .ToList();


            List<Transacao> transacoesCredito = dbContext.Transacao.Where(transaction => transaction.ContaId == contaId && transaction.TipoTransacao.Descricao == "Crédito")
                                                                   .ToList();

            double totalCredito = 0;

            foreach (Transacao creditos in transacoesCredito) {
                totalCredito += creditos.Valor;    
            }

            return totalCredito;
        }
        
        public double GetTotalDebito(int contaId)
        {


            //List<Transacao> transacoesDebito = transacaoList.Where(transaction => transaction.ContaId == contaId && transaction.TipoTransacao.Descricao == "Débito")
            //                                                      .ToList();

            List<Transacao> transacoesDebito = dbContext.Transacao.Where(transaction => transaction.ContaId == contaId && transaction.TipoTransacao.Descricao == "Débito")
                                                                  .ToList();

            double totalDebito = 0;

            foreach (Transacao debitos in transacoesDebito)
            {
                totalDebito += debitos.Valor;
            }

            return totalDebito;
        }

        public double GetSaldoContabilistico(int contaId)
        {
            return GetTotalCredito(contaId) - GetTotalDebito(contaId);
        }

        public List<Transacao> GetAllFromLastDays(int days, int account)
        {
            DateTime lastDays = DateTime.Now.AddDays(-days);

            return (List<Transacao>) GetAll().Where(transaction => {
                bool isTransactionInBetweenDays = transaction.Dia.CompareTo(lastDays) >= 0 && transaction.Dia.CompareTo(DateTime.Now) <= 0;

                bool isTransactionFromAccount = transaction.ContaId == account;

                return isTransactionInBetweenDays && isTransactionFromAccount;
            }).ToList();
        }

        public int Create(int contaId, int tipoTransacaoId, DateTime data, Double valor)
        {
            BusContaBancaria conta = new BusContaBancaria();
            BusTipoTransacao tipotransacao = new BusTipoTransacao();
            int result = 0;

            if (conta.GetId(contaId) != null && tipotransacao.GetId(tipoTransacaoId) != null)
            {
                Transacao transacao = new Transacao(contaId, tipoTransacaoId, data, valor);
                dbContext.Transacao.Add(transacao);
                result = dbContext.SaveChanges();                
            }

            return result;
           
        }
    }
}