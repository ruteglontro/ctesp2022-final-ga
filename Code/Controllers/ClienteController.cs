﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Code.Models;
using Code.Bus;

namespace Code.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly ILogger<ClienteController> _logger;
        private BusCliente bus = new BusCliente();

        public ClienteController(ILogger<ClienteController> logger)
        {
            _logger = logger;
        }


        [HttpGet]
        public IActionResult Get()
        {
            List<Cliente> match = bus.Get();

            if(match != null)
            {
                return Ok(match);
            }
            else
            {
                return NoContent();
            }
        }


        [HttpGet("{clientid}")]
        public IActionResult GetId(int clientid)
        {
            IActionResult result;

            var match = bus.GetId(clientid);

            result = (match != null) ? Ok(match) : NotFound();

            return result;
        }

        [HttpPost]
        public IActionResult Create(Cliente cliente)
        {

            IActionResult result;

            if (bus.Create(cliente))
            {
                result = Ok();
            }
            else
            {
                result = Conflict();
            }

            return result;
        }

        [HttpPut("{clientid}")]
        public IActionResult Update(int clientid, Cliente cliente)
        {
            IActionResult result;

            
            if (bus.Update(clientid, cliente))
            {
                result = Ok();
            }
            else
            {
                result = NotFound();
            }

            return result;
        }

        [HttpDelete("{clientid}")]
        public IActionResult Delete(int clientid)
        {
            IActionResult result;

            if (bus.Delete(clientid))
            {
                result = Ok();
            }
            else
            {
                result = NotFound();
            }

            return result;
        }
    }
   }
