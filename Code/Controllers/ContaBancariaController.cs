﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Code.Models;
using Code.Bus;

namespace Code.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContaBancariaController : ControllerBase
    {

        private readonly ILogger<ContaBancariaController> _logger;
        private BusContaBancaria bus;

        public ContaBancariaController(ILogger<ContaBancariaController> logger)
        {
            _logger = logger;

            bus = new BusContaBancaria();

        }


        [HttpGet]
        public IActionResult Get()
        {

            return Ok(bus.Get());
        }


        [HttpGet("{contaid}")]
        public IActionResult GetId(int contaid)
        {
            IActionResult result;

            var match = bus.GetId(contaid);

            result = (match != null) ? Ok(match) : NotFound();

            return result;
        }

        [HttpPost]
        public IActionResult Create(ContaBancaria contaBancaria)
        {

            IActionResult result;

            if (bus.Create(contaBancaria))
            {
                result = Ok(201);
            }
            else
            {
                result = Conflict();
            }

            return result;
        }

        [HttpPut("{contaid}")]
        public IActionResult Update(int contaid, ContaBancaria ContaBancaria)
        {
            return (bus.Update(contaid, ContaBancaria)) ? Ok() : NotFound();
        }

        [HttpDelete("{contaid}")]
        public IActionResult Delete(int contaid)
        {
            return (bus.Delete(contaid)) ? Ok() : NotFound();
        }
    }
}
