﻿using Code.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Code.Bus;

namespace Code.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoTransacaoController : ControllerBase
    {
        private readonly ILogger<TipoTransacaoController> _logger;
        private BusTipoTransacao bus;

        public TipoTransacaoController(ILogger<TipoTransacaoController> logger)
        {
            _logger = logger;

            bus = new BusTipoTransacao();

        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(bus.Get());
        }

        [HttpGet("{id}")]
        public IActionResult GetId(int id)
        {

            TipoTransacao match = bus.GetId(id);

            if (match != null) {
                return Ok();
            }
            else {
                return NotFound();
            }

        }

        [HttpPost]
        public IActionResult Create(TipoTransacao tipoTransacao)
        {

            if (bus.Create(tipoTransacao))
            {
                return CreatedAtAction("Create", tipoTransacao);
            }
            else
            {
                return UnprocessableEntity();
            }
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, TipoTransacao tipoTransacao)
        {
            if (id != tipoTransacao.TipoId)
            {
                return BadRequest();
            }

            if (bus.Update(id, tipoTransacao))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return (bus.Delete(id)) ? Ok() : NotFound();
        }

    }
}
