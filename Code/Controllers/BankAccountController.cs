﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Code.Bus;
using Code.Models;

namespace Code.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BankAccountController : ControllerBase
    {
        private readonly ILogger<BankAccountController> _logger;

        private BusContaBancaria busConta;
        private BusCliente busCliente;
        private BusTransacao busTransacao;
        private BusTipoTransacao busTipoTransacao;

        public BankAccountController(ILogger<BankAccountController> logger)
        {
            _logger = logger;

            busConta = new BusContaBancaria();  
            busCliente = new BusCliente(); 
            busTransacao = new BusTransacao();
            busTipoTransacao = new BusTipoTransacao();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id) { 
            ContaBancaria conta = busConta.GetId(id);
            return (conta != null) ? Ok(conta) : NoContent();
        }

        [HttpGet("/{id}/transaction")]
        public IActionResult GetTransaction(int id){
            List<Transacao> list = busTransacao.GetAllFromLastDays(30, id);
            return (list != null) ? Ok(list) : NotFound();   
        }

        [HttpGet("/all/balanco-diario")]
        public IActionResult GetBalancoDiario(int id)
        {
            Dictionary<int, List<Transacao>> balancoDiarioPorConta = new Dictionary<int, List<Transacao>>();

            busConta.Get().ForEach(contaBancaria =>
               {
                   balancoDiarioPorConta.Add(contaBancaria.ContaId, busTransacao.GetAllFromLastDays(1, contaBancaria.ContaId));
               }
            );

            return (balancoDiarioPorConta.Count == 0) ? NoContent() : Ok(balancoDiarioPorConta);
        }

        [HttpPost]
        public IActionResult Create(int contaId, int tipoTransacaoId, DateTime data, Double valor){
            int nAffectedRows = busTransacao.Create(contaId, tipoTransacaoId, data, valor);

            return (nAffectedRows > 0) ? Ok() : NotFound();
        }
    }
}
