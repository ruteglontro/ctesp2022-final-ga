﻿using Code.Intro;
using Code.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Code.Bus;

namespace Code.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransacaoController : ControllerBase
    {
        private readonly ILogger<TransacaoController> _logger;
        private BusTransacao bus = new BusTransacao();

        public TransacaoController(ILogger<TransacaoController> logger)
        {
            _logger = logger;                                 
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id){
            IActionResult result;

            Transacao transacao = bus.GetTransacao(id);

            result = (transacao == null) ? NoContent() : Ok(transacao);
            
            return result;
        }

        [HttpGet]
        public IActionResult GetAllFromLastDays(int account, int days){
    
            List<Transacao> transactionsInLastDays;

            transactionsInLastDays = bus.GetAllFromLastDays(account, days);

            return Ok(transactionsInLastDays);
        }

        [HttpPost]
        public IActionResult Create(int contaId, int transactionTypeId, DateTime day, double value){
            IActionResult result;

            int res = bus.Create(contaId, transactionTypeId, day, value);

            if(res == 0 ){
                // fail
                result = Ok(false);
            }
            else{
                result = Ok(true);
            }

            return  result;
        }
    }
}
