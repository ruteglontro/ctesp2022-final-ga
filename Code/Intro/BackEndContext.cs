﻿using Code.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Code.Intro
{
    public class BackEndContext : DbContext
    {

        public DbSet<Cliente> Cliente{ get; set; }
        public DbSet<ContaBancaria> ContaBancaria{ get; set; }
        public DbSet<Transacao> Transacao { get; set; }
        public DbSet<TipoTransacao> TipoTransacao { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
            optionsBuilder.UseSqlServer(@"Server=localhost,5434;Database=DevOps;User Id=sa;Password=Vagrant42");
        }
    }
}
