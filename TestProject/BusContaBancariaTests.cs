using Code.Bus;
using Code.Controllers;
using Code.Models;
using NUnit.Framework;

namespace Tests
{
    public class BusContaBancariaTests
    {
        BusContaBancaria busContaBancaria = new BusContaBancaria();

        [Test]
        public void TestGetId()
        {
            Assert.IsInstanceOf(typeof(ContaBancaria), busContaBancaria.GetId(-1));
        }

        [Test]
        public void TestCreate()
        {
            //Total em d�bito na conta, ou seja, valor em d�vida que o utilizador ir� posteriormente ser descontado no valor em cr�dito;
            ContaBancaria contaBancariaValida = new ContaBancaria(-4, -1, -2, "000000000", 0f);
            ContaBancaria contaBancariaInValida = new ContaBancaria(-1, -1, -2, "000000000", 0f);

            Assert.IsTrue(busContaBancaria.Create(contaBancariaValida));
            Assert.IsFalse(busContaBancaria.Create(contaBancariaInValida));
        }

        [Test]
        public void TestUpdate()
        {
            //�Saldo contabil�stico�, � a diferen�a entre o cr�dito e o d�bito;
            ContaBancaria contaBancaria = new ContaBancaria(-1, -1, -2, "000000000", 10f);
            Assert.IsTrue(busContaBancaria.Update(contaBancaria.ContaId, contaBancaria));
        }

        [Test]
        public void TestDelete()
        {
            Assert.IsTrue(busContaBancaria.Delete(-4));
            Assert.IsFalse(busContaBancaria.Delete(-4));
        }

        /*
        [Test]
        public void TestBalancoDiario()
        {
            //Balan�o Di�rio�, neste campo deve ser poss�vel verificar o total gasto e o total recebido na conta por cliente;
            int obterNosUltimosDias = 1;
            Assert.AreEqual(4, busTransacao.GetAllFromLastDays(obterNosUltimosDias, contaIdTeste).Count);
        }*/
       
    }
}