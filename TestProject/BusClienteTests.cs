using Code.Bus;
using Code.Controllers;
using Code.Models;
using NUnit.Framework;

namespace Tests
{
    public class BusClienteTests
    {
        BusCliente busCliente = new BusCliente();

        [Test]
        public void TestGetId()
        {
            Assert.IsInstanceOf(typeof(Cliente), busCliente.GetId(-1));
        }

        [Test]
        public void TestCreate()
        {
            Cliente clienteValido = new Cliente(-3, "Jose Antunes", "Rua das Flores, 111", "Conta ordenado");

            Assert.IsTrue(busCliente.Create(clienteValido));

            // Tentar inserir um duplicado
            Assert.IsFalse(busCliente.Create(clienteValido)); 
        }

        [Test]
        public void TestUpdate()
        {
            Cliente clienteValido = new Cliente(-1, "Ant�nio Santos", "Rua 31 de Janeiro, Porto", "Conta poupanca");

            Assert.IsTrue(busCliente.Update(clienteValido.ClienteId, clienteValido));
        }

        [Test]
        public void TestDelete()
        {
            Assert.IsTrue(busCliente.Delete(-3));
            Assert.IsFalse(busCliente.Delete(-3)); // Tentar eliminar registo inexistente
        }
  
    }
}