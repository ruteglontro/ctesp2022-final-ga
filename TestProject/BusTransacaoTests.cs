using Code.Bus;
using Code.Controllers;
using NUnit.Framework;

namespace Tests
{
    public class BusTransacaoTests
    {
        BusTransacao busTransacao = new BusTransacao();
        private int contaIdTeste = -1;


        [Test]
        public void TestTotalCredito()
        {
            //Total de saldo na conta, ou seja, valor positivo que poder� ser gasto pelo utilizador da conta;
            Assert.AreEqual(80, busTransacao.GetTotalCredito(contaIdTeste));

        }

        [Test]
        public void TestTotalDebito()
        {
            //Total em d�bito na conta, ou seja, valor em d�vida que o utilizador ir� posteriormente ser descontado no valor em cr�dito;
            Assert.AreEqual(20, busTransacao.GetTotalDebito(contaIdTeste));        
        }

        [Test]
        public void TestSaldoContabilistico()
        {
            //�Saldo contabil�stico�, � a diferen�a entre o cr�dito e o d�bito;
            Assert.AreEqual(60, busTransacao.GetSaldoContabilistico(contaIdTeste));
        }

        [Test]
        public void TestGetAllFromLastDays()
        {
            int obterNosUltimosDias = 30;

            Assert.AreEqual(4, busTransacao.GetAllFromLastDays(obterNosUltimosDias, contaIdTeste).Count);
        }

        [Test]
        public void TestBalancoDiario()
        {
            //Balan�o Di�rio�, neste campo deve ser poss�vel verificar o total gasto e o total recebido na conta por cliente;
            int obterNosUltimosDias = 1;
            Assert.AreEqual(4, busTransacao.GetAllFromLastDays(obterNosUltimosDias, contaIdTeste).Count);
        }
       
    }
}