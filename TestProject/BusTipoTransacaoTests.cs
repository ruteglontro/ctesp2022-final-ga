using Code.Bus;
using Code.Controllers;
using Code.Models;
using NUnit.Framework;

namespace Tests
{
    public class BusTipoTransacaoTests
    {
        BusTipoTransacao busTipoTransacao = new BusTipoTransacao();


        [Test]
        public void TestGetId()
        {
            Assert.IsInstanceOf(typeof(TipoTransacao), busTipoTransacao.GetId(-1));
        }

        [Test]
        public void TestCreate()
        {
            //Total em d�bito na conta, ou seja, valor em d�vida que o utilizador ir� posteriormente ser descontado no valor em cr�dito;
            TipoTransacao novoTipoTransacaoValido = new TipoTransacao(-3, "Um novo tipo");
            TipoTransacao novoTipoTransacaoDuplicado = new TipoTransacao(-1, "Duplicado");


            Assert.IsTrue(busTipoTransacao.Create(novoTipoTransacaoValido));
            Assert.IsFalse(busTipoTransacao.Create(novoTipoTransacaoDuplicado));
        }

        [Test]
        public void TestUpdate()
        {
            //�Saldo contabil�stico�, � a diferen�a entre o cr�dito e o d�bito;
            TipoTransacao tipoTransacao = new TipoTransacao(-1, "Alterado");
            TipoTransacao tipoTransacaoInexistente = new TipoTransacao(-5, "Nao existe");

            Assert.IsTrue(busTipoTransacao.Update(tipoTransacao.TipoId, tipoTransacao));
            
            // Testar onde ids sao diferentes
            Assert.IsFalse(busTipoTransacao.Update(-2, tipoTransacao));

            // Testar onde nao existem registos
            Assert.IsFalse(busTipoTransacao.Update(tipoTransacaoInexistente.TipoId, tipoTransacaoInexistente));
        }

        [Test]
        public void TestDelete()
        {
            Assert.IsTrue(busTipoTransacao.Delete(-3));
            Assert.IsFalse(busTipoTransacao.Delete(-3));
        }

    }
}